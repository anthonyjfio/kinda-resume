var metalsmith = require('metalsmith'),
    markdown   = require('metalsmith-markdown'),
    stylus     = require('metalsmith-stylus'),
    templates  = require('metalsmith-templates');

metalsmith(__dirname)
  .use(markdown())
  .use(stylus())
  .use(templates('jade'))
  .destination('./build')
  .build(function(err) {
    if (err) {throw err; }
});
