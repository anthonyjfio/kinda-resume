---
template: index.jade
---
## Kinda Resume

I've been trying to learn HTML/CSS and Javascript by myself for about the past year now. I like to thing I'm making progress but my projects this far have only been for myself. This was kinda thrown together using a static site generator that I've been learning to use called [Metalsmith](http://www.metalsmith.io/). I used markdown to type this out, jade to render the rest of the html and stylus for the css.

#### Accomplishments/Progress

* Finished [Codecademy](http://www.codecademy.com/tracks/web) HTML/CSS track
* Finished [Codecademy](http://www.codecademy.com/tracks/jquery) jQuery track
* Finished [Codecademy](http://www.codecademy.com/tracks/javascript) Javascript track
* Finished [Codecademy](http://www.codecademy.com/en/skills/make-a-website) Make a Website project
* Built [anthonyfiorani.com](http://anthonyfiorani.com/) using HTML/CSS and the [Hakyll](http://jaspervdj.be/hakyll/) static site generator

#### Curently Working On

Re-Doing my personal site using [Metalsmith](http://www.metalsmith.io/). Here's my [code](https://gitlab.com/ajfiorani/siteReDo/tree/pagination-blog-collection-normal-home) for it so far. [This](http://codepen.io/ajfiorani/full/RPbNML/) is roughly what it should look like when I'm finished.

#### Other Skills

* I'm pretty comfortable using Linux. [Arch linux](https://www.archlinux.org/) is currently what I'm using for my laptop and server.
* Git
* Command Line
* Running a VPS

If you have any other questions you can get my number through Tom.
